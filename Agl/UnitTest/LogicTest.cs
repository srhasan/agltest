﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Agl;
using Agl.Entity;
using System.Threading.Tasks;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class LogicTest
    {

        private string _jsonResponse = @"[
{
""name"": ""Bob"",
""gender"": ""Male"",
""age"": 23,
""pets"": [
{
""name"": ""Garfield"",
""type"": ""Cat""
},
{
""name"": ""Fido"",
""type"": ""Dog""
}
]
},
{
""name"": ""Jennifer"",
""gender"": ""Female"",
""age"": 18,
""pets"": [
{
""name"": ""Garfield"",
""type"": ""Cat""
}
]
},
{
""name"": ""Steve"",
""gender"": ""Male"",
""age"": 45,
""pets"": null
},
{
""name"": ""Fred"",
""gender"": ""Male"",
""age"": 40,
""pets"": [
{
""name"": ""Tom"",
""type"": ""Cat""
},
{
""name"": ""Max"",
""type"": ""Cat""
},
{
""name"": ""Sam"",
""type"": ""Dog""
},
{
""name"": ""Jim"",
""type"": ""Cat""
}
]
},
{
""name"": ""Samantha"",
""gender"": ""Female"",
""age"": 40,
""pets"": [
{
""name"": ""Tabby"",
""type"": ""Cat""
}
]
},
{
""name"": ""Alice"",
""gender"": ""Female"",
""age"": 64,
""pets"": [
{
""name"": ""Simba"",
""type"": ""Cat""
},
{
""name"": ""Nemo"",
""type"": ""Fish""
}
]
}
]";

        /// <summary>
        /// A basic test to show mocking some dependency.
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task AfterSorting_Person_With_NoMatching_Pet_Should_Be_Excluded()
        {
            var mock = new Mock<IHttpClientService>();
            mock.Setup(x => x.GetJsonResponse()).ReturnsAsync(_jsonResponse);

            var logicLayer = new PeopleLogic(mock.Object);

            var r = await logicLayer.GetPeopleWithPet(PetTypes.Fish);

            Assert.IsTrue( r.ToList().Count == 1, "Should result 1 match only");
        }
    }
}
