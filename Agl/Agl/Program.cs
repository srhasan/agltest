﻿using Agl.Entity;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Agl
{
    class Program
    {
        /*
         
A json web service has been set up at the url: http://agl-developer-test.azurewebsites.net/people.json

You need to write some code to consume the json and 
    output a list of all the cats in 
    alphabetical order under a heading of the gender of their owner.

Example:

Male

Angel
Molly
Tigger

Female

Gizmo
Jasper
      
             
             */


        static void Main(string[] args)
        {
            var client = new HttpClient();
            var endpoint = "http://agl-developer-test.azurewebsites.net/people.json"; // can come from config file
            IUnityContainer container = new UnityContainer();
            container.RegisterType<IHttpClientService, HttpClientService>(new InjectionConstructor(new HttpClient(), endpoint ));
            
            var logic = container.Resolve<PeopleLogic>();
            
            var sortedList = logic.GetPeopleWithPet(PetTypes.Cat).Result;

            Output(sortedList);
            
            Console.ReadLine();
                                   
        }

        private static void Output(IEnumerable<Person> people)
        {
            var lastGenderType = GenderTypes.Unknown;
            
            // some basic console output            
            foreach (var person in people)
            {
                if (person.Gender != lastGenderType)
                {
                    lastGenderType = person.Gender;
                    Console.WriteLine("-------------");
                    Console.WriteLine(lastGenderType.ToString());
                    Console.WriteLine("-------------");
                }

                foreach (var pet in person.pets)
                {
                    Console.WriteLine(pet.Name);
                }
            }
        }  

    }
}
