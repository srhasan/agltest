﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Agl
{
    public interface IHttpClientService
    {
        Task<string> GetJsonResponse();
    }

    public class HttpClientService : IHttpClientService
    {
        private readonly HttpClient _client;
        private readonly string _endPoint;
        
        public HttpClientService (HttpClient client, string endPoint)
        {
            _client = client;
            _endPoint = endPoint;
        }

        public async Task<string> GetJsonResponse()
        {
            HttpResponseMessage response = await _client.GetAsync(_endPoint);
            response.EnsureSuccessStatusCode(); // In more realistic scenerio we will be checking status codes and response content for more appropriate action
            return await response.Content.ReadAsStringAsync();
        }

       

    }
}
