﻿using Agl.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Agl
{
    public interface IPeopleLogic
    {
        Task<IEnumerable<Person>> GetPeople();
        Task<IEnumerable<Person>> GetPeopleWithPet(PetTypes petType);
    }
    public class PeopleLogic: IPeopleLogic
    {
        private readonly JsonSerializerSettings _settings;
        private readonly IHttpClientService _client;

        public PeopleLogic(IHttpClientService client)
        {
            _client = client;
            _settings = new JsonSerializerSettings {
                
                NullValueHandling = NullValueHandling.Ignore // this simplifies the linq by relying that ctor will initialize the List
            }; 
        }
        public async Task<IEnumerable<Person>> GetPeople()
        {
            string jsonResponse = await _client.GetJsonResponse();
            return await Task.Factory.StartNew(() => 
                JsonConvert.DeserializeObject<IEnumerable<Person>>(jsonResponse, _settings).ToList());

        }

        public async Task<IEnumerable<Person>> GetPeopleWithPet(PetTypes petType)
        {
            var people = await GetPeople();
            // slightly more convulated linq assuming we don't want to pass the list multiple times
            // Enum assumes we have an agreed set of values, if not we ll have to use string comparer or custom json converter
            return people
                .OrderBy(x => x.Gender)
                .Select(x => new Person
                {
                    Gender = x.Gender, Age = x.Age, Name = x.Name,
                    pets = x.pets.OrderBy(pet => pet.Name).Where(pet => pet.Type == petType).ToList() 
                })
                .Where(x => x.pets.Any());
        }


        }
}
