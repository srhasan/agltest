﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Agl.Entity
{
    //Enum assumes we have an agreed set of values, if not we ll have to use string comparer or custom json converter
    public enum GenderTypes
    {
        Unknown,
        Male,
        Female
    }

    //Enum assumes we have an agreed set of values, if not we ll have to use string comparer or custom json converter
    public enum PetTypes
    {
        Fish,
        Cat,
        Dog
    }
    
    public class Person
    {
        public Person()
        {
            pets = new List<Pet>();
        }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        //Enum assumes we have an agreed set of values, if not we ll have to use string comparer or custom json converter
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "gender")]
        public GenderTypes Gender { get; set; }

        [JsonProperty(PropertyName = "age")]
        public int Age { get; set; }

        public IEnumerable<Pet> pets { get; set; }
    }

    public class Pet
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        //Enum assumes we have an agreed set of values, if not we ll have to use string comparer or custom json converter
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "type")]
        public PetTypes Type { get; set; }
    }
}
